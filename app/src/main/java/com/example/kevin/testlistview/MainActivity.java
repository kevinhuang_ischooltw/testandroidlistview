package com.example.kevin.testlistview;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList<StudentInfo> data ;
    private ListView listView ;
    private Activity act = this;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getData();
        setContentView(R.layout.activity_main);

        this.listView = (ListView)this.findViewById(R.id.listView);

        myAdapter adp = new myAdapter(this, this.data);
        this.listView.setAdapter(adp);

        this.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                StudentInfo stud = data.get(position);
                Intent intent = new Intent( act, SecondActivity.class );
                intent.putExtra("studName", stud.name);
                act.startActivity(intent);
            }
        });
    }


    private void getData() {
        this.data = new ArrayList<StudentInfo>();
        this.data.add(new StudentInfo("Bill", 20, "101", "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcSBEw6H2oFXtIMusYHyYDEaq2VhMK0e8VuH6ZSxNfo1YjCwP5i2"));
        this.data.add(new StudentInfo("Zoe", 30, "101", "https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcSBEw6H2oFXtIMusYHyYDEaq2VhMK0e8VuH6ZSxNfo1YjCwP5i2"));
        this.data.add(new StudentInfo("YM", 40, "101","https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcSBEw6H2oFXtIMusYHyYDEaq2VhMK0e8VuH6ZSxNfo1YjCwP5i2"));
    }

    //Declare a custom Adapter
    private class myAdapter extends  ArrayAdapter<StudentInfo> {

        private ArrayList<StudentInfo> students ;
        private Activity context;

        public myAdapter(Activity context, ArrayList<StudentInfo> students) {
            super(context, R.layout.my_list_item, students);
            this.students = students;
            this.context = context ;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater  inflater = context.getLayoutInflater();
            View itemView = inflater.inflate(R.layout.my_list_item, null, true);

            StudentInfo stud = this.students.get(position);
            TextView txtTitle = (TextView)itemView.findViewById(R.id.item_title);
            TextView txtDesc = (TextView)itemView.findViewById(R.id.item_description);
            ImageView img = (ImageView)itemView.findViewById(R.id.imageView);

            txtTitle.setText(stud.name);
            txtDesc.setText(stud.age + "");

            //非同步載入圖片 ....

            return itemView;
        }
    }

    //Declare a custom class to store data.
    private class StudentInfo {
        public String name;
        public int age;
        public String className;
        public String photoUrl;

        public StudentInfo(String name, int age, String className, String photoUrl) {
            this.name = name;
            this.age = age;
            this.className = className;
            this.photoUrl = photoUrl;
        }
    }

}


