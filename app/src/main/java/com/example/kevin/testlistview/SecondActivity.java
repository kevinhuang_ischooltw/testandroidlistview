package com.example.kevin.testlistview;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class SecondActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        Intent sourceIntent = getIntent();
        String studName = sourceIntent.getStringExtra("studName");
        Toast.makeText(this, studName, Toast.LENGTH_SHORT);
    }
}
